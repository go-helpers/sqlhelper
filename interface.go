package sqlhelper

// Query interface builder for query
type Query interface {
	Columns(...string) Query
	WithCondition(Condition) Query
	WithJoin(...Statement) Query
	GroupBy(...string) Query
	OrderBy(...string) Query
	Limit(int) Query
	Offset(int) Query
	Build() (string, error)
}

// Condition condition interface
type Condition interface {
	And(...string) Condition
	Or(...string) Condition
	Build() (string, error)
}

// Statement type
type Statement string

func (s Statement) String() string {
	return string(s)
}

// Query type definition
const (
	QuerySelect = "SELECT"
	QueryUpdate = "UPDATE"
	QueryInsert = "INSERT"
)

// JoinType definition
const (
	LeftJoin  = "LEFT JOIN"
	InnerJoin = "INNER JOIN"
	RightJoin = "RIGHT JOIN"
)

// conditions const declaration
const (
	ConditionAnd     = "AND"
	ConditionOr      = "OR"
	ConditionIn      = "IN"
	ConditionNotNull = "IS NOT NULL"
	ConditionNull    = "IS NULL"
)
