package mysqlaid_test

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-helpers/sqlhelper/mysqlaid"
)

func TestQueryBuilder_Select(t *testing.T) {

	builder := mysqlaid.NewSelect("table t")
	builder.Columns("t.id", "t.name")
	builder.Limit(5)
	builder.Offset(0)

	query1, err := builder.Build()
	assert.Nil(t, err)
	fmt.Println("query1", query1)

	builder.WithJoin(mysqlaid.LeftJoin("posts p", "p.poster_id=t.id"))
	query1a, err := builder.Build()
	assert.Nil(t, err)
	fmt.Println("query1a", query1a)

	condition := mysqlaid.NewConditionBuilder("t.gender=?")
	condition.And("active=?")
	builder.WithCondition(condition)
	query2, err := builder.Build()
	assert.Nil(t, err)
	fmt.Println("query2", query2)

	builder.GroupBy("age", "country")
	builder.WithJoin(mysqlaid.InnerJoin("archives a", "p.poster_id=t.id"))
	query3, err := builder.Build()
	assert.Nil(t, err)
	fmt.Println("query3", query3)
}
