package mysqlaid

import (
	"fmt"
	"strings"

	"gitlab.com/go-helpers/sqlhelper"
)

// ConditionBuilder implements Condition
type ConditionBuilder struct {
	conditions []string
}

// NewConditionBuilder initializes new query builder
func NewConditionBuilder(condition string) sqlhelper.Condition {
	return &ConditionBuilder{
		conditions: []string{condition},
	}
}

//And and condition
func (b *ConditionBuilder) And(args ...string) sqlhelper.Condition {
	for _, arg := range args {
		b.conditions = append(b.conditions, sqlhelper.ConditionAnd)
		b.conditions = append(b.conditions, arg)

	}
	return b
}

// Or or condition
func (b *ConditionBuilder) Or(args ...string) sqlhelper.Condition {
	for _, arg := range args {
		b.conditions = append(b.conditions, sqlhelper.ConditionOr)
		b.conditions = append(b.conditions, arg)

	}
	return b
}

// Build builds querystring
func (b *ConditionBuilder) Build() (s string, e error) {
	s = fmt.Sprintf("WHERE %s", strings.Join(b.conditions, " "))

	return
}
