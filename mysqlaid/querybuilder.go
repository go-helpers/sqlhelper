package mysqlaid

import (
	"errors"
	"fmt"
	"strings"

	"gitlab.com/go-helpers/sqlhelper"
)

// QueryBuilder struct that implements Query
type QueryBuilder struct {
	queryType     string
	tableName     string
	attributes    []string
	joins         []string
	condition     sqlhelper.Condition
	groupBy       []string
	orderBy       []string
	limit, offset *int
}

// newQuery initializes new query builder
func newQuery(qType, table string) sqlhelper.Query {
	return &QueryBuilder{
		queryType: qType,
		tableName: table,
	}
}

//NewSelect initializes select query
func NewSelect(table string) sqlhelper.Query {
	return newQuery(sqlhelper.QuerySelect, table)
}

//NewUpdate initializes select query
func NewUpdate(table string) sqlhelper.Query {
	return newQuery(sqlhelper.QueryUpdate, table)
}

//NewInsert initializes select query
func NewInsert(table string) sqlhelper.Query {
	return newQuery(sqlhelper.QueryInsert, table)
}

// Columns columns
func (b *QueryBuilder) Columns(attributes ...string) sqlhelper.Query {
	b.attributes = attributes
	return b
}

// WithJoin includes joins
func (b *QueryBuilder) WithJoin(args ...sqlhelper.Statement) sqlhelper.Query {
	for _, arg := range args {
		b.joins = append(b.joins, arg.String())
	}
	return b
}

// WithCondition ...
func (b *QueryBuilder) WithCondition(conditon sqlhelper.Condition) sqlhelper.Query {
	b.condition = conditon
	return b
}

// GroupBy sets group by attrubites
func (b *QueryBuilder) GroupBy(args ...string) sqlhelper.Query {
	b.groupBy = args
	return b
}

// OrderBy sets order by attrubites
func (b *QueryBuilder) OrderBy(args ...string) sqlhelper.Query {
	return b
}

// Limit sets limit
func (b *QueryBuilder) Limit(limit int) sqlhelper.Query {
	b.limit = &limit
	return b
}

// Offset sets offset
func (b *QueryBuilder) Offset(offset int) sqlhelper.Query {
	b.offset = &offset
	return b
}

// Build builds querystring
func (b *QueryBuilder) Build() (s string, e error) {
	if len(b.attributes) == 0 {
		return "", errors.New("at least 1 attribute must be declared")
	}
	switch b.queryType {
	case sqlhelper.QueryInsert:
	case sqlhelper.QuerySelect:
		var (
			attributesString = strings.Join(b.attributes, ", ")
		)

		base := "SELECT %s from %s"
		s = fmt.Sprintf(base, attributesString, b.tableName)

		if len(b.joins) != 0 {
			joinString := strings.Join(b.joins, " ")
			s = fmt.Sprintf("%s %s", s, joinString)
		}
		if b.condition != nil {
			var conditionString string
			if conditionString, e = b.condition.Build(); e != nil {
				return "", e
			}
			s = fmt.Sprintf("%s %s", s, conditionString)
		}

		if len(b.groupBy) != 0 {
			groupString := fmt.Sprintf("GROUP BY %s", strings.Join(b.groupBy, ", "))
			s = fmt.Sprintf("%s %s", s, groupString)
		}

		if b.offset != nil {
			offsetString := fmt.Sprintf("OFFSET %v", *b.offset)
			s = fmt.Sprintf("%s %s", s, offsetString)
		}
		if b.limit != nil {
			limitString := fmt.Sprintf("LIMIT %v", *b.limit)
			s = fmt.Sprintf("%s %s", s, limitString)
		}

	case sqlhelper.QueryUpdate:
	}
	s = fmt.Sprintf("%s;", s)
	return
}
