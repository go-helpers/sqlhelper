package mysqlaid

import (
	"fmt"
	"strings"

	"gitlab.com/go-helpers/sqlhelper"
)

func join(jointype, tableName, conditionstring string) (s sqlhelper.Statement) {
	return sqlhelper.Statement(fmt.Sprintf("%s %s ON %s", jointype, tableName, conditionstring))
}

// LeftJoin builds join statement
func LeftJoin(tableName, condition string) sqlhelper.Statement {
	return join(sqlhelper.LeftJoin, tableName, condition)
}

// InnerJoin builds join statement
func InnerJoin(tableName, condition string) sqlhelper.Statement {
	return join(sqlhelper.InnerJoin, tableName, condition)
}

// RightJoin builds join statement
func RightJoin(tableName, condition string) sqlhelper.Statement {
	return join(sqlhelper.RightJoin, tableName, condition)
}

// ConditionIn .. where `column` IN (?...)
func ConditionIn(column string, size int) (s sqlhelper.Statement) {
	str := make([]string, size)
	for i := range str {
		str[i] = "?"
	}
	return sqlhelper.Statement(
		fmt.Sprintf(
			"%s IN (%s)",
			column,
			strings.Join(str, ", "),
		),
	)
}

// ConditionNotNull where `column` IS NOT NULL
func ConditionNotNull(column string) sqlhelper.Statement {
	return sqlhelper.Statement(fmt.Sprintf("%s %s", column, sqlhelper.ConditionNotNull))
}
