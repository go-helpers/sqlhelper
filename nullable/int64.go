package nullable

import (
	"database/sql"
	"encoding/json"
)

type Int64 struct {
	sql.NullInt64
}

func NewInt64(n int64) Int64 {
	return Int64{
		sql.NullInt64{
			Valid: true,
			Int64: n,
		},
	}
}

func (ni Int64) MarshalJSON() ([]byte, error) {
	if ni.Valid {
		return json.Marshal(ni.Value)
	}
	return json.Marshal(nil)
}

func (ni Int64) UnmarshalJSON(data []byte) error {
	var i *int64
	if err := json.Unmarshal(data, &i); err != nil {
		return err
	}

	ni.Valid = false
	if i != nil {
		ni.Valid = true
		ni.Int64 = *i
	}
	return nil
}
