package nullable

import (
	"database/sql"
	"encoding/json"
)

type Float64 struct {
	sql.NullFloat64
}

func NewFloat64(n float64) Float64 {
	return Float64{
		sql.NullFloat64{
			Valid:   true,
			Float64: n,
		},
	}
}

func (ni Float64) MarshalJSON() ([]byte, error) {
	if ni.Valid {
		return json.Marshal(ni.Value)
	}
	return json.Marshal(nil)
}

func (ni Float64) UnmarshalJSON(data []byte) error {
	var i *float64
	if err := json.Unmarshal(data, &i); err != nil {
		return err
	}

	ni.Valid = false
	if i != nil {
		ni.Valid = true
		ni.Float64 = *i
	}
	return nil
}
