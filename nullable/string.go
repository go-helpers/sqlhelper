package nullable

import (
	"database/sql"
	"encoding/json"
)

type String struct {
	sql.NullString
}

func NewString(n string) String {
	return String{
		sql.NullString{
			Valid:  true,
			String: n,
		},
	}
}

func (ni String) MarshalJSON() ([]byte, error) {
	if ni.Valid {
		return json.Marshal(ni.Value)
	}
	return json.Marshal(nil)
}

func (ni String) UnmarshalJSON(data []byte) error {
	var i *string
	if err := json.Unmarshal(data, &i); err != nil {
		return err
	}

	ni.Valid = false
	if i != nil {
		ni.Valid = true
		ni.String = *i
	}
	return nil
}
