package nullable

import (
	"database/sql"
	"encoding/json"
)

type Bool struct {
	sql.NullBool
}

func NewBool(v bool) Bool {
	return Bool{
		sql.NullBool{
			Valid: true,
			Bool:  v,
		},
	}
}

func (ni Bool) MarshalJSON() ([]byte, error) {
	if ni.Valid {
		return json.Marshal(ni.Value)
	}
	return json.Marshal(nil)
}

func (ni Bool) UnmarshalJSON(data []byte) error {
	var i *bool
	if err := json.Unmarshal(data, &i); err != nil {
		return err
	}

	ni.Valid = false
	if i != nil {
		ni.Valid = true
		ni.Bool = *i
	}
	return nil
}
